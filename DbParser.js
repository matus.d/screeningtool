const { Client} = require('pg')

class DBParser {

    constructor() {
        const dbString = process.env.DATABASE_URL || 'postgres://gbzgqhzhmksnob:44d340e2888043672bf63845dbda787ce32e03ccf35de35c864020135e5c8a18@ec2-54-75-238-138.eu-west-1.compute.amazonaws.com:5432/dbvekoicimhnl0'
        
        this.client = new Client({
            connectionString: dbString,
            ssl: true
        })
    }

    async getAllCustomers() {
        this.client.connect();
        try {
            const { rows } = await this.client.query('SELECT * FROM customers ORDER BY lastname ASC')
            this.client.end();
            return rows;
        } catch (err) {
            return {
                type: 'error',
                message: err.message
            }
        }
    }

    async updateCustomer(email, newStatus) {
        this.client.connect();
        try {
            const { rows } = await this.client.query(`UPDATE customers 
                                                        SET status=$2 
                                                        WHERE email=$1
                                                        RETURNING email, status`, [email, newStatus])
            this.client.end();
            return rows;
        } catch (err) {
            return {
                type: 'error',
                message: err.message
            }
        }
    }

}

module.exports = DBParser;
