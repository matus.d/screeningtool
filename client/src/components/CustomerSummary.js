import React from 'react'
import linkedinLogo from '../img/linkedin_logo.png'
import '../css/styles.css'
import { DropdownButton, Dropdown, ButtonGroup } from 'react-bootstrap'

const CustomerSummary = ({customer, updateHandler}) => {

    const dateString = new Date(customer.created_dttm).toLocaleDateString("en-UK", {dateStyle : 'medium', timeStyle: 'short'});
    const emailMailtoStrig = `mailto:${customer.email}`;

    const handleClick = (newStatus) => {
        updateHandler(newStatus, customer.email)
    }

    return (
        <tr>
            <td>
                <img src={customer.picture} alt='profile'></img>               
            </td>
            <td>
                {customer.firstname} {customer.lastname}
                <br></br>
                <a href={customer.linkedin}>
                    <img src={linkedinLogo} alt='linkedin' className='linkedinLogo'></img>
                </a>
            </td>
            <td>
                <DropdownButton as={ButtonGroup} title={customer.status} id="bg-nested-dropdown" className={customer.status} >
                    <Dropdown.Item onClick={() => handleClick('approved')} className='textApproved'>Approve</Dropdown.Item>
                    <Dropdown.Item onClick={() => handleClick('declined')} className='textDeclined'>Decline</Dropdown.Item>
                </DropdownButton>
            </td>
            <td>
                <a href={emailMailtoStrig} target='_top'>{customer.email}</a>
            </td>
            <td>{dateString}</td>
        </tr>
    )
}

export default CustomerSummary