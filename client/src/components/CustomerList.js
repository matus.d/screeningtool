import React from 'react';
import CustomerSummary from './CustomerSummary';
import { Table } from 'react-bootstrap';

const CustomerList = ({customers, updateHandler}) => {
    return (
        <Table responsive striped bordered hover size="sm">
            <thead>
                <tr>
                <th></th>
                <th>Name</th>
                <th>Status</th>
                <th>Email</th>
                <th>Date Created</th>
                </tr>
            </thead>
            <tbody>
            { customers && customers.map( (customer, i) => {
                return (                    
                    <CustomerSummary customer={customer} updateHandler={updateHandler} key={i}></CustomerSummary>
                )
            })}
            </tbody>
        </Table>
    )
}

export default CustomerList