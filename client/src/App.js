import React, { Component } from 'react';
import './App.css';
import CustomerList from './components/CustomerList'
import { Navbar, Container, Row, Col } from 'react-bootstrap';
import './css/styles.css';

class App extends Component {

  state = {
    data: null
  };

  componentDidMount() {
    this.getCustomers()
      .then(res => this.setState({ data: JSON.parse(res.data) }))
      .catch(err => console.log(err));
  }
  
  getCustomers = async () => {
    const response = await fetch('/customers');
    const body = await response.json();

    if (response.status !== 200) {
      throw Error(body.message) 
    }
    return body;
  };

  updateCustomer = async (newStatus, email) => {
    const response = await fetch('/updateCustomer', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({newStatus, email})
    });
    const body = await response.json();

    if (response.status !== 200) {
      throw Error(body.message) 
    }

    const newData = JSON.parse(body.data)

    if(newData){

      const { email, status } = newData[0]

      const newDataState = this.state.data.map( customer => { 
        if(customer.email === email){
          customer.status = status;
        }
        return customer;      
      })

      this.setState({ data: newDataState });
    }
   
  };

  render() {
    return (
      <div className="App">
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand>
            {' Screening Tool'}
          </Navbar.Brand>
        </Navbar>
        <Container>
          <Row>
            <Col>
              <CustomerList customers={this.state.data} updateHandler={this.updateCustomer} className="full-table"></CustomerList>
            </Col>
          </Row>          
        </Container>            
      </div>
    );
  }
}

export default App;
