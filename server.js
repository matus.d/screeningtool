const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const path = require('path');
const DbParser = require('./DbParser');
const bodyParser = require('body-parser')

app.listen(port, () => console.log(`Listening on port ${port}`));
app.use(bodyParser.json())

app.get('/customers', async (req, res) => {
  const parserInstance = new DbParser();

  const result = await parserInstance.getAllCustomers()
  res.send({ data: JSON.stringify(result) });
});

app.post('/updateCustomer', async (req, res) => {
  const parserInstance = new DbParser();

  const { newStatus, email } = req.body;

  const result = await parserInstance.updateCustomer(email, newStatus);
  res.send({ data: JSON.stringify(result) });
});

if (process.env.NODE_APP === 'production' || process.env.NODE_APP === 'dev') {
  app.use(express.static('client/build'));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}
