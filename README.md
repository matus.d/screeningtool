# ScreeningTool
Requirements:
- Node.js backend API
- React frontend
Task:
- List all applicant profiles on a single page.
- Format data on the page as you think best suits the app. Use any CSS framework you wish, however please make sure you include custom CSS somewhere.
- Add an "Accept" and "Decline" button for each applicant profile, this should update a "status" column in the database. (If an applicant has been accepted or declined then the profile should show it, but also allow someone to change their decision and choose another option.)
Once complete please:
- Host the app somewhere and send us the url.
- Send the code to us in a zipped folder (make sure to provide the .sql database export file with the code).